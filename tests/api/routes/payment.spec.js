const request = require("supertest");
const mongoose = require("mongoose");

const { MongoMemoryServer } = require("mongodb-memory-server");

const createApp = require("../../../api/app");

let config, app, mongod;

beforeAll(async (done) => {
  mongod = new MongoMemoryServer();

  const uri = await mongod.getUri();
  config = {
    db: {
      dbUrl: uri,
      mongoose,
    },
  };

  app = await createApp(config);

  done();
});

afterAll(async (done) => {
  await mongoose.connection.close();
  await mongod.stop();
  done();
});

describe("Payment Routes", () => {
  it("should get all payments", function (done) {
    request(app)
      .post("/payments")
      .set("Accept", "application/json")
      .set("Content-Type", "application/json")
      .send({ customer: 1, amount: 10 })
      .expect(201)
      .end(function (err, res) {
        if (err) return done(err);
        console.log(res.body);
        done();
      });
  });
});
